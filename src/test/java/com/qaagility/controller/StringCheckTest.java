package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;

public class StringCheckTest {

    @Test
    public void testGstr() throws Exception {
	String input = "Hello";
        StringCheck stringCheck = new StringCheck(input);

        String result = stringCheck.gstr();

        assertEquals("does not contain hello", input, result);
    }

   @Test
   public void testGstrWithNullString() throws Exception {
        StringCheck stringCheck = new StringCheck(null);

        String result = stringCheck.gstr();

        assertNull("string is null", result);
    }
}
