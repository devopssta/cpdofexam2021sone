package com.agiletestingalliance;

import static org.junit.Assert.*;
import org.junit.Test;


public class MinMaxTest {

	@Test
	public void testMax() throws Exception{
		MinMax minMax = new MinMax();

		int result1 = minMax.max(5, 3);
		assertEquals("should return 5", 5, result1);

		int result2 = minMax.max(2, 7);
	        assertEquals("should return 7", 7, result2);

		int result3 = minMax.max(4, 4);
        	assertEquals("should return 4", 4, result3);
    }

@Test
public void testBar() throws Exception{
	MinMax minMax = new MinMax();
	
	String result1 = minMax.bar("Hello");
        assertEquals("Hello", result1);
	
	String result2 = minMax.bar("");
        assertEquals("", result2);

	String result3 = minMax.bar(null);
        assertEquals("", result3);
    }
}
