package com.agiletestingalliance;

public class MinMax {

	public int max(int firstNum, int secondNum) {
		return Math.max(firstNum, secondNum);
	}

	public String bar(String string) {
		if (string != null && !string.isEmpty()) {
			return string;
		}
		return "";
	}

}

